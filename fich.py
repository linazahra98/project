import numpy as np
    import random
    import tkinter as tk
    window = tk.Tk()
    window.title('Probabilités Paradoxe de Parrondo')
    window.iconbitmap('C:\\Users\\acer\\Downloads\\folder_games_22301.ico')
    window['bg'] = '#4CC4C4'
    frame = tk.Frame(master=window,relief=tk.SUNKEN,borderwidth=3)
    frame.grid(row=0 , column=0, padx=5 , pady=5)
    labeltx = tk.Label(master=frame,text='JEUX_A')
    labeltx.grid(row=0, column=0, padx=5 , pady=5)
    window.geometry('600x200')
    #img = tk.PhotoImage(file='C:\\Users\\acer\\Downloads\\img1.png')
    #label = tk.Label(master=frame,image = img, width=100,height=100)
    #labeL.grid(row=0, column=0, padx=5 , pady=5)
    btn = tk.Button(text='click',command=tirage_jeu_A)
    btn.grid(row=3 , column=3 ,padx=3 , pady=3)
    window.mainloop()
   
    def tirage_jeu_A():
        x = random.randint(0,1)
        if x < 0.49:
            return + 1
        else:
            return - 1
    
    def gain_jeu_A(N):
        g=0
        for i in range(N):
            g = g + tirage_jeu_A()
        return g
      

    def esperance_jeu_A(N):
        esperance = gain_jeu_A(N)/N
        return esperance
    #TESTA
    print("GAME_A")
    N = 1000000
    print(esperance_jeu_A(N))
    
    def tirage_jeu_B(g):
        if g % 3 == 0:
            if random.random() <= 0.09:
              return  + 1
            else:
              return  - 1
        else:
            if random.random() <= 0.74:
              return   + 1
            else:
              return   - 1
    def gain_jeu_B(N):
        gb=0
        for J in range(N):
            gb = gb + tirage_jeu_B(gb)
        return gb
    
    
    def esperance_jeu_B(N):
        esperance = gain_jeu_B(N)/N
        return esperance
    #TESTB
    print("GAME_B")
    N = 1000000
    print(esperance_jeu_B(N))
    
    def tirage_jeu_AB(g):
        if random.random()< 0.5:
            return tirage_jeu_A()
        else:
            return tirage_jeu_B(g)

    def gain_jeu_AB(N):
        g=0
        for i in range(N):
            g = g + tirage_jeu_AB(g)
        return g
    def esperance_jeu_AB(N):
        esperance = gain_jeu_AB(N)/N
        return esperance
    #TESETAB
    print("GAME_AB")
    N = 1000000
    print(esperance_jeu_AB(N))
    
        